// Iteration Methods, loops through all the elements to perform repatitive tasks on the array

// forEach() - to loop through the array
// map () -  loops through the array and returns a new array
// filter - it returns a new array containing elements which meets the given condition

// every() - checls of all elements meet the given condition
let numbers = [1, 2, 3, 4, 5, 6];

let allValid = numbers.every(function(number) {
 return number > 3;
})

console.log("result of every() method: ")
console.log(allValid)

let someValid = numbers.some(function(number){
	return number < 2;
})

console.log('result of some () method: ')
console.log(someValid);

// includes() method - methods can be "chained" using them one after another
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a')
})

console.log(filteredProducts)

// reduce()
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('current iteration: '+ ++iteration);
	console.log('accumulator: '+ x);
	console.log('currentValue: '+ y);

	return x + y;
})

console.log('result of reduce method: '+ reducedArray);

let productsReduce = products.reduce(function(x, y){

	return x + ' ' + y
})

console.log('result of reduce() method: ' + productsReduce);


function getProductNames(products) {
  const productNames = products.map(product => product.name);
  return productNames;
}

console.log(getProductNames);