const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');




module.exports.registerUser = (request_body) => {


	let new_user = new User({
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			})
		}

	
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}

module.exports.getAllUsers = (request, response) => {
	return User.find({}).then (result => {
		return response.send(result);
	})
}

module.exports.getDetails = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		user.password = "";
		return user;
	}).catch(error => console.log(error))
}

module.exports.getAdmin = (request, response) => {
	return User.findByIdAndUpdate({_id: request.body.id},{isAdmin: "true"},{new:true}).then((result, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send("This user is now an Admin")
	})
}


module.exports.removeAdmin = (request, response) => {
	return User.findByIdAndUpdate({_id: request.body.id},{isAdmin:'false'},{new:true}).then((result, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send("This user is now remove as Admin")
	})
}

