const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


module.exports.addProduct = (request_body) => {
	let new_product = new Product ({
		name: request_body.name,
		description: request_body.description,
		price: request_body.price
	});

	return new_product.save().then((saved_product, error) => {
		if(error){
			return {
				message:fase
			};
		}
		return{
			message:'Successfully added'
		};
	}).catch(error => console.log(error));
	
}		

module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then (result => {
		return response.send(result);
	})
}

module.exports.getProducts = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}

module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive:true}).then(result => {
		return response.send(result);
	})
}





module.exports.updateProduct = (request, response) => {
	let update_product_details ={
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	return Product.findByIdAndUpdate(request.params.id, update_product_details).then((course, error) => {
		if (error){
		return response.send ({
			message: error.message
		})
	}

	return response.send({
			message: 'Course has been updated successfully!'
		})
	})
}


module.exports.archiveProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, {isActive: false}).then((product, error) => {
	if(error){
		return response.send (false)
	}


	return response.send (true);
})
}

module.exports.activateProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, {isActive: true}).then((product, error) => {
	if(error){
		return response.send (false)
	}


	return response.send (true);
})
}

