const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const Product = require('../models/Product.js');
const User = require('../models/User.js');

//create order
module.exports.createOrder = async (request, response) => {

    if (request.user.isAdmin) {
      return response.send('Action Forbidden');
	}

    const { products } = request.body;
    const userId = request.user.id;

    let totalAmount = 0;
    for (const product of products) {
      const productData = await Product.findById(product.productId);
      if (!productData) {
        return response.send({ message: `Product with ID ${product.productId} not found` });
      }

      if (!productData.price || !product.quantity) {
    	return response.send({ message: `Invalid product data for ID ${product.productId}` });
  	  }

      totalAmount += productData.price * product.quantity;
    }


    let new_order = new Order({
      userId: userId,
      products: products,
      totalAmount: totalAmount,
    });

   return new_order.save().then((saved_order, error) => {
   	if(error){
   		return response.send({
   			message: error.message
   		})
   	}
   	return response.send({
   		message: 'Order created successfully'
   	})
  }).catch(error => console.log(error))

}

