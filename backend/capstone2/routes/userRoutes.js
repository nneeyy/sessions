const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

//register
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})

//login
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})
//details
router.post('/details', auth.verify, (request,response) => {
	UserController.getDetails(request.body).then((result) => {
		response.send(result);
	})
})

// get all user
router.get('/all', (request, response) => {
	UserController.getAllUsers(request, response);
})


// user Admin
router.put('/:id/admin', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getAdmin(request, response);
})
// remove Admin
router.post('/:id/removeadmin', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getRemoveAdmin(request, response);
});


module.exports = router;