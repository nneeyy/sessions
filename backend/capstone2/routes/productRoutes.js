const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');


//create product
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request.body).then((result) => {
		response.send(result);
	})
})

// get all product
router.get('/all', (request, response) => {
	ProductController.getAllProducts(request, response);
});




// Get all active product
router.get('/allactive', (request,response) => {
	ProductController.getAllActiveProducts(request, response);
})


//specific product
router.get('/:id', (request,response) => {
	ProductController.getProducts(request, response);
})

// update
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})


//archive
router.put('/:id/archive', auth.verify, (request, response) => {
	ProductController.archiveProduct(request, response);
})

// activate single product
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response);
})


module.exports = router;