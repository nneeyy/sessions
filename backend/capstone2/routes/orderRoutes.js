const express = require('express');
const router = express.Router();
const orderController = require('../controllers/OrderController.js');
const auth = require('../auth.js');

// create order
router.post('./create', auth.verify, (request,response) => {
	OrderController.createOrder(request, response);
})


module.exports = router;