const mongoose = require('mongoose');


const	product_schema = new mongoose.Schema({
name: {
		type: String,
		required: [true, 'Name is required!']
	},
	description: {
		type: String,
		required: [true, 'password is required!']
	},
	
	 price : {
        type : Number,
        required : [true, "Price is required"]
	},
	isAactive: {
		type: Boolean,
		default: true,
	},
	 createdOn : {
        type : Date,
        default : new Date()
    }
})	
module.exports = mongoose.model('Product', product_schema);

