const mongoose = require('mongoose');


const	user_schema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	password: {
		type: String,
		required: [true, 'password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cart: [
		{
		productId: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Product'
		},
		quantity: {
			type: Number,
			default: 1,
		}
	
		}
	]

});
module.exports = mongoose.model('User', user_schema);