const mongoose = require('mongoose');


const	order_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'Name is required!']
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, 'productid is required!']
			},

			quantity : {
        		type : Number,
        		required : [true, "amount is required"]
			}
		}	
		],
		totalAmount : {
        type : Number,
        required : [true, "amount is required"]
	},
	 purchasedOn : {
        type : Date,
        default : new Date()
    }
})
module.exports = mongoose.model('Order', order_schema);