const Task = require('../models/Task.js');

module.exports.getAllTasks = () => {
	return Task.find({}).then((result, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		return {
			tasks: result
		}
	})
}

module.exports.createTask = (request_body) => {
	Task.findOne({name: request_body.name}).then((result, error) => {
		// if the the task already exists by utilizing the 'name' property. if it does, then return a response a 
		if(result != null && result.name == request_body.name){
			return {
				message: "Duplicate task found!"
			};
		} else {
			
			let newTask = new Task({
				name: request_body.name
			});
	
			return newTask.save().then((savedTask, error) => {
				if(error){
					return {
						message: error.message
					};
				}

				return {
					message:'New task created!'
			};
		})
	}
	})
}