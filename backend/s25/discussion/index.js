// Pokemon constructor function
function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = 50 + 10 * level; // Equation to calculate health based on level
  this.attack = 5 + 2 * level;   // Equation to calculate attack based on level

  // Method to tackle another Pokemon
  this.tackle = function(target) {
    console.log(`${this.name} is tackling ${target.name}!`);
    target.health -= this.attack; // Reduce the target's health by the attacker's attack value
    if (target.health <= 0) {
      this.faint(target);
    }
    return `${this.name} tackled ${target.name}.`;
  };

  // Method to handle fainting
  this.faint = function(target) {
    console.log(`${target.name} has fainted!`);
  };
}

// Trainer object using object literals
const trainer = {
  name: "Ash Ketchum",
  age: 15,
  pokemon: [],
  friends: {
    close: ["Misty", "Brock"],
    distant: ["Professor Oak", "Gary"]
  },

  // Method to talk
  talk: function() {
    return "Pikachu! I choose you!";
  }
};

// Access trainer object properties using dot and square bracket notation
console.log(trainer.name);
console.log(trainer["age"]);

// Invoke the trainer's talk method
console.log(trainer.talk());

// Instantiate several Pokemon objects from the constructor with varying name and level properties
const pikachu = new Pokemon("Pikachu", 8);
const charmander = new Pokemon("Charmander", 6);
const bulbasaur = new Pokemon("Bulbasaur", 7);

// Invoke the tackle method of one Pokemon object to see if it works as intended
console.log(pikachu.tackle(charmander));

// Add the Pokemon objects to the trainer's Pokemon array
trainer.pokemon.push(pikachu, charmander, bulbasaur);