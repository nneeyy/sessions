// Function Declaration & Invocation
// function printName(){
// 	console.log("My name is Jeff");
// }

// printName();

// // FUNCTION EXPRESSION
// let variable_function = function(){
// 	console.log ("Hello from function expression!")
// }

// variable_function();

// SCOPING
let global_variable = "Call me mr. worldwide";

console.log(global_variable);

function showNames(){
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);
	// you can use global variables inside any function as long as thery are declared outside of the function scope.
	console.log(global_variable);
}

// You CANNOT use locally-scoped variables outside the function they are declared in.



showNames();


// NESTED FUNCTIONS
function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John"

		console.log(name);

		// Accesing the 'nested_name' variable within the same function it was declared in, WIll work.
		console.log(nested_name);
	}

	childFunction();

	// Accessing the 'nested_name' variable OUTSIDE the function it was declared in, WILL NOT work.
	
}

parentFunction();


// BEST PRACTICE FOR FUNCTION NAMING
// function printWelcomeMessageForUser(){
// 	let first_name = prompt("Enter your first name: ");
// 	let last_name = prompt ("Enter your last name: ");

// 	console.log("Hello, " + first_name + " " + last_name + "!");
// 	console.log("Welcome sa page ko!");
// }

// printWelcomeMessageForUser();


// RETURN STATEMENT
function fullName(){
	return "Neo Yumang";

}
console.log(fullName());