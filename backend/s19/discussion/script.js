// Javascript consists of whats called 'statements'. and statements are basically just syntax with semi-colons at the end.
// alert("Hello World!");

// Javascript can access the 'log' function of the console to display text/data in the console.
console.log("Hello World!");

// [SECTION] variables


// variable declaration & Invocation
let my_variable = "Hola Mundo!";
console.log(my_variable);

// concatenating strings
let country = "Philippines";
let province = "Metro Manila";

let full_address = province + ',' + country;

console.log(full_address);

let headcount = 26;
let grade = 98.7;

console.log("the number of student is " headcount + " and the average grade of all student is " + grade);

// let sum = headcount + grade;

// console.log(sum);

// boolean - the value of boolean is only true or false. when naming variable that have a boolean value, make sure they're formatted like a question.
let is_married = false;
let is_good_conduct = true;

console.log("He's married: " + is_married);
console.log("She's a good person: " + is_good_conduct);

// Arrays
Let grades = [98.7, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
let person = {
	fullName: "Juan Dela Cruz",
	age: 40,
	isMarried: false,
	contact: ["09977114455", "0977442563"],
	address: {
		houseNumber: "345";
		city: "England"
	}
}


// Javascript reads arrays as objects. this is mainly to accomodate for specific functionalities that array can do later on.
console.log(typeof person);
console.log(typeof grades);

// Null & Undefined
let girlfriend = null;
let full_name;

console.log(girlfriend);
console.log(full_name);

// [section] operators

// arithmetic operators
let first_number = 5;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;

console.log("Sum: " + sum)
console.log("Difference: " + difference)
console.log("Product: " + product)
console.log("Quotient: " + quotient)
console.log("Remainder: " + remainder)


// assignment
assignment_number = assignment_number + 2;
console.log ()