const express = require('express');
const router = express.Router();
const auth = require ('../auth.js');
const CourseController = require('../controllers/CourseController.js');

// You can destructure an auth variable to extract the function being exported from it. You can then use the functions directly without having to use dot(.) notation.
// const {verify, verifyAdmin} = auth;

// Insert routes here

// Create single course
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.addCourse(request, response)
});

// get all courses
router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response);
});

// Get all active courses
router.get('/', (request,response) => {
	CourseController.getAllActiveCourses(request, response);
})


// Get single course
router.get('/:id', (request, response) => {
	CourseController.getCourse(request, response);
})

router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response);
})



router.put('/:id/archive', auth.verify, (request, response) => {
	CourseController.archiveCourse(request, response);
})

// activate single course
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.activateCourse(request, response);
})

// search  course by name
router.post('/search', (request, response) => {
	CourseController.searchCourses(request, response);
});
module.exports = router;
