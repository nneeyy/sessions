//[Section] if-else statements

// let number =  1;

// if(number > 1){
// 	console.log("the number is greater than 1!");
// } else if (number < 1){
// 	console.log("the number is less than 1!");
// }else {
// 	console.log("None of the conditions were true")
// }

// // Falsey values
// if(false){
// 	console.log("Falsey");
// }

// if(0){
// 	console.log("Falsey");
// }

// if(undefined){
// 	console.log("Falsey");
// }

// // Truthy values
// if(true){
// 	console.log("Truthy");
// }

// if(1){
// 	console.log("Truthy");
// }

// if([]){
// 	console.log("Truthy");
// }


// // Ternary Operators
// let result = (1 < 10) ? true : false;

// // This is the if-else equivalent of the ternary operation above.
// // if(1 < 10){
// // 	return true;
// // }else {
// // 	return false;
// // }

// // If there are multiple line within the if-else block, it's better to use the regular if-else syntax as the ternary operator is only capable of handling one-liners.
// if(5 == 5){
// 	let greeting = "hello";
// 	console.log(greeting);
// }

// console.log("Value returned from the ternary operator is " + result);

// //[SECTION] Switch Statement
// let day = prompt ("what day of the week is today? ").toLowerCase();

// switch(day){
// 	case 'monday':
// 		console.log("The day today is monday!");
// 		break;
// 	case 'tuesday':
// 		console.log("The day today is tuesday!");
// 		break;
// 	case 'wednesday':
// 		console.log("The day today is wednesday!");
// 		break;	
// 	case 'thursday':
// 		console.log("The day today is thursday!");
// 		break;
// 	case 'friday':
// 		console.log("The day today is friday!");
// 		break;		
// 	default:
// 		console.log("Please input a valid day naman pareh");
// 		break
// }

// // [SECTION] try/Catch/Finally Statements
// function showIntensityAlert(windspeed){
// 	try{
// 		alerat(determineTyhpoonIntensity(windspeed));
// 	}catch(error){
// 		console.log(error.message)
// 	}finally{
// 		alert("Intensity updated will show new alert!");
// 	}
// }

// showIntensityAlert(56);

function checkAverage(avg) {
  if (avg >= 60 && avg <= 69) {
    return "Hello, student, your average is " + avg + ". The letter equivalent is D";
  } else if (avg >= 70 && avg <= 79)
  function checkAverage(avg) {
    return "Hello, student, your average is " + avg + ". The letter equivalent is C";
  } else if (avg >= 80 && avg <= 84) {
    return "Hello, student, your average is " + avg + ". The letter equivalent is B-";
  } else if (avg >= 85 && avg <= 89) {
    return "Hello, student, your average is " + avg + ". The letter equivalent is B";
  } else if (avg >= 90 && avg <= 95) {
    return "Hello, student, your average is " + avg + ". The letter equivalent is A";
  } else if (avg > 96) {
    return "Hello, student, your average is " + avg + ". The letter equivalent is A+";
  } else {
    return "Hello, student, please enter a valid average.";
  }
}

// Example usage

console.log(checkAverage(97));
// function checkAverage(num1, num2, num3, num4, num5) {
// 		let result = num1 * num2;
  

// 	console.log("the product:", num1 + " and " + num2 + " is ");
// 	return result

// }
