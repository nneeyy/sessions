// [SECTION] while loop
// let count = 5;

// while (count !== 0){
// 	console.log("Current value of count: " + count);
// 	count--;
// }

// [SECTION] Do-while loop
// let number = Number (prompt("Give me a number:"));
// do {
// 	console.log("Current value of number: " + number);

// 	number += 1;
// } while (number <= 10)


// // [SECTION] for Loop
// for (let count = 0; count <= 20; count++){
// 	console.log("Current for loop value: " + count);
// }

// let my_string = "Neo";

// // to get the length of a string
// console.log(my_string.length);

// // to get a specific letter in a string
// console.log(my_string[0]);


// // Loops through each letter in the string and will keep iterating as long as the current index is less than the lengh of the string.
// for (let index = 0; index < my_string.length; index ++){
// 	console.log(my_string[index]); 
// }

// MINI ACTIVITY (20mins)
// 1.loop through the 'my_name' variable which has a string with your name on it.
// 2. Display each letter in the console but exclude all the vowels from it.
// let my_name = "Neo Yumang"


// for (let index = 0; index < my_name.length; index ++) {
// 	if(
//   my_name[index].toLowerCase() == "a" ||
//   my_name[index].toLowerCase() == "e" ||
//   my_name[index].toLowerCase() == "i" ||
//   my_name[index].toLowerCase() == "o" ||
//   my_name[index].toLowerCase() == "u" 
//   ){
// // console.log("");
// continue;  //if we use 'continue' keyword, it will skip the else block and reiterate the loop to check if the next letter is a vowel.
// } else {
// 	console.log(my_name[index]);
// }
// }

// let name_two = "Cloud";

// for (Let index = 0; index < name_two.length; index++){
	
// 	if (name_two[index].toLowerCase() == "o"){
// 		console.log("Skipping...");
// 		continue;
// 	}

// 	if(name_two[index] == "u"){
// 		break;
// 	}
// console.log(name_two[index])
// }

// Step 1: Get the number from the user
const number = parseInt(prompt("Enter a number:"));

// Step 2: Loop to print numbers divisible by 5 and not divisible by 10
let currentNumber = number;
while (currentNumber >= 50) {
  if (currentNumber % 10 === 0) {
    console.log("The number is being skipped:", currentNumber);
  } else if (currentNumber % 5 === 0) {
    console.log("Divisible by 5:", currentNumber);
  }
  currentNumber--;
}

// Step 3: Get the string from the user
const inputString = prompt("Enter a string:");

// Step 4: Filter consonants from the string
let filteredString = "";
const vowels = "aeiouAEIOU";
for (let i = 0; i < inputString.length; i++) {
  const letter = inputString[i];
  if (vowels.includes(letter)) {
    continue; // If it's a vowel, continue to the next iteration
  } else {
    filteredString += letter; // If it's a consonant, add it to the filteredString
  }
}

// Step 5: Print the filtered consonants
console.log("Filtered consonants from the string:", inputString);
console.log((filteredString));